<style media="screen">
  .commentlist{padding: 0}
  .comment{border-bottom: 1px solid #ddd; padding-bottom: 15px; margin-bottom: 15px}
  .comment:last-child{border-bottom: 0px solid #ddd;}
  .comment-author cite{font-weight: 700}
  .comment-author img{border-radius: 50%}
  .comment-meta a{color: #777; font-size: 0.8em}
  .comment-meta {}
  .comment-body p{margin-bottom: -5px; margin-top: -5px}
  .children{list-style: none;margin-top: 10px;}
  .children li{border-bottom: 0;padding-bottom: 0; margin-bottom: 0}
</style>


<h3>Comentarios</h3>
    <?php
    $mensajes = wp_count_comments(get_the_ID());
    ?>
    <p><strong><?php echo $mensajes->approved; ?> Respuestas</strong></p>


    <ul class="commentlist" style="list-style:none">
    	<?php
    		//Gather comments for a specific page/post
        $id_proyecto = get_the_ID();
        $lista_comentarios = get_approved_comments($id_proyecto);
    		$comments = get_comments(array(
    			'status' => 'approve', //Change this to the type of comments to be displayed
          'post_id' => get_the_ID(),
    		));

    		//Display the list of comments
    		wp_list_comments(array(
    			'per_page' => 10, //Allow comment pagination
    			'reverse_top_level' => false //Show the oldest comments at the top of the list
    		), $comments);
    	?>
    </ul>
    <?php
    /**wp_list_comments( $args, $comments );**/
    $comments_args = array(
            // change the title of send button
            'label_submit'=>'Enviar',
            // change the title of the reply section
            'title_reply'=>'Escribe tu comentario',
            // remove "Text or HTML to be displayed after the set of comment fields"
            'comment_notes_after' => '',
            // redefine your own textarea (the comment body)
            'comment_field' => '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label><br /><textarea id="comment" name="comment" aria-required="true"></textarea></p>',

    );

    comment_form($comments_args);
?>
